
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="https://www.indohotels.id/asset/img/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
    <link href="https://www.indohotels.id/asset/bootstrap/css/bootstrap.css?" rel="stylesheet" type="text/css" media="all">
    <link href="https://www.indohotels.id/asset/css/vendor/magnific-popup.css" rel="stylesheet" type="text/css" media="all">
    <link href="https://www.indohotels.id/asset/css/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="https://www.indohotels.id/asset/css/vendor/jquery-ui.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="https://www.indohotels.id/asset/css/main.css?v=2.4.1" rel="stylesheet" type="text/css" media="all">
    <link href="https://www.indohotels.id/asset/css/addon.min.css?v=2.4.1" rel="stylesheet" type="text/css" media="all">
    <link href="https://www.indohotels.id/asset/css/icons.css?v=2.4.1" rel="stylesheet" type="text/css" media="all">

    <script type="text/javascript" src="https://www.indohotels.id/asset/js/vendor/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="https://www.indohotels.id/asset/js/vendor/jquery-ui.min.js"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <title>Payment Status</title>
</head>
<body itemscope itemtype="http://schema.org/Hotel" >
    	<!-- Google Code for indohotels.id Conversion Page -->
	<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 861137392;
		var google_conversion_language = "en";
		var google_conversion_format = "3";
		var google_conversion_color = "ffffff";
		var google_conversion_label = "rzg0CPD56XUQ8NPPmgM";
		var google_remarketing_only = false;
		/* ]]> */
		</script>
			<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
			<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/861137392/?label=rzg0CPD56XUQ8NPPmgM&amp;guid=ON&amp;script=0"/>
		</div>
	</noscript>
    <div id="overlay"></div>
        <a href="#top" id="toTop"></a>
<header id="header" class="public clearfix">
    
</header>
<div id="content" class="wrapper static-payment">
    <div class="container">
        <div id="wrapper-ajx" class="row">
                                    <div class="col-md-12">
                <div class="payment-status">
                    <h3>Waiting for payment : <span class="red-text" style="color:#dc252b;white-space: nowrap;">Bank Transfer </span></h3>
                </div>
                <hr>
                                <div class="payment-items">
                    <ul>
                        <li>
                            <div class="items">

                                <table>
                                    <tr>
                                        <td>CIMB Virtual Account Number</td>
                                        <td><span class="dot">:</span></td>
                                        <td><span class="pull-right"> 4549181535294594</span></td>
                                    </tr>
                                                                        <tr>
                                        <td>Transfer Nominal</td>
                                        <td><span class="dot">:</span></td>
                                        <td>IDR <span class="pull-right">&nbsp;1,170,000</span></td>
                                    </tr>

                                </table>
                            </li>
                        </ul>
                    </div>
                </div>
                                <div class="payment-description">
                    <strong>Payment Expired in : </strong> <h2 id='clock'></h2>
                </div>
                <div class="payment-description">
                    <h4>Non Refundable</h4>
                </div>
                <div class="col-md-12">
                    <h3>Transaction Details</h3><br>
                    <div class="payment-items">
                        <ul>
                                                        <li>
                                <div class="items">
                                    <h4>Eastparc Hotel Yogyakarta</h4>
                                                                        <table>
                                        <tr class="checkin">
                                            <td valign="top">Check-in </td>
                                            <td valign="top"><span class="dot">:</span></td>
                                            <td valign="top" align="right">Sunday, 26-08-2018</td>
                                        </tr>
                                        <tr>
                                            <td valign="top">Check-out </td>
                                            <td valign="top"><span class="dot">:</span></td>
                                            <td valign="top" align="right">Monday, 27-08-2018</td>
                                        </tr>
                                        <tr>
                                            <td valign="top">Room(s)</td>
                                            <td valign="top"><span class="dot">:</span></td>
                                            <td valign="top" align="right">
                                                <p>1 Premier Twin Room</p>                                            </td>
                                        </tr>
                                                                                <tr>
                                            <td><b>Before taxes</b></td>
                                            <td><span class="dot">&nbsp;</span></td>
                                            <td>&nbsp;</td>
                                        </tr>
                                                                                <tr class="line">
                                            <td>Room price </td>
                                            <td><span class="dot">:</td>
                                            <td class="currency">IDR <span class="pull-right"> 966,942</td>
                                        </tr>
                                                                                <tr class="line">
                                            <td><b>Subtotal</b></td>
                                            <td><span class="dot">:</td>
                                            <td>IDR <span class="pull-right"> 966,942 </td>
                                        </tr>

                                        <tr class="line">
                                            <td><b>Taxes</b></td>
                                            <td><span class="dot">:</td>
                                            <td>IDR <span class="pull-right">203,058</td>
                                        </tr>
                                        <tr class="line">
                                            <td><b>Subtotal</b></td>
                                            <td><span class="dot">:</td>
                                            <td>IDR <span class="pull-right"> 1,170,000 </td>
                                        </tr>

                                        <tr class="line total">
                                            <td><b>Price Including taxes</b></td>
                                            <td><span class="dot">&nbsp;</span></td>
                                            <td>&nbsp;</td>
                                        </tr>
                                                                                                                                                                                                          <tr class="odd" mc:repeatable style="background: #fff; padding-bottom: 3px;">
                                            <td><b>Total payable</b></td>
                                            <td><span class="dot">:</span></td>
                                            <td>IDR <span class="pull-right"> 1,170,000</td>
                                        </tr>
                                                                                                                                                                                                                                                                                                                            </table>
                                </div>
                            </li>
                                                    </ul>
                    </div><br><br>
                    <div class="row payment-description">
                                                <div class="row">
                            <div class="col-md-2 hidden-sm"></div>
                            <div class="col-md-8 col-xs-12 contentinfo_outro text-left"><strong>Payment Guideline : </strong>
                                <ol>
                                    <li>
                                        You must transfer the correct amount of your total price to the bank account.                                    </li>
                                    <li>
                                        Payment through CIMB Niaga ATM :                                         <ul>
                                            <li> Choose other transaction </li>
                                            <li> Choose virtual account </li>
                                            <li> Enter your virtual account number </li>
                                            <li> Follow the next instructions on ATM </li>
                                        </ul>
                                    </li>
                                    <li>
                                        Payment through Internet Banking CIMB Clicks :                                         <ul>
                                            <li> Login into your CIMB Clicks account </li>
                                            <li> Choose paybills </li>
                                            <li> Choose payment type </li>
                                            <li> Choose virtual account </li>
                                            <li> Enter your virtual account number </li>
                                            <li> Follow the next instructions </li>
                                        </ul>
                                    </li>
                                    <li>
                                        Payment through other bank ATM (ATM Bersama) :                                         <ul>
                                            <li> Choose transfer to other bank </li>
                                            <li> Enter CIMB Niaga bank code 022 or choose CIMB Niaga then followed by your virtual account number </li>
                                            <li> Follow the next instructions </li>
                                        </ul>
                                    </li>
                                    <li>
                                        Payment through Other Bank Online Internet Banking :                                         <ul>
                                            <li> Choose menu online transfer to other bank </li>
                                            <li> Enter CIMB Niaga bank code 022 or choose CIMB Niaga then followed by your virtual account number on beneficiary account </li>
                                            <li> Follow next instructions </li>
                                        </ul>
                                    </li>
                                </ol>
                            </div>
                            <div class="col-md-2 hidden-sm"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 hidden-sm"></div>
                            <div class="col-md-8 col-xs-12 contentinfo_outro text-left"><strong>Important Informations : </strong>
                                <ol>
                                    <li>
                                       Please be informed that you will be charged for IDR 6,500 - IDR 7,500 per transaction, not including in the total amount of your booking for payment using bank transfer via ATM Bersama or other bank services.<br>
                                    </li>
                                    <li>
                                        Payment using CIMB account will be free of charges.<br>
                                    </li>
                                    <li>
                                        We will send a confirmation letter for your successful payment and your e-voucher will be send directly to your email.<br>
                                    </li>
                                    <li>
                                        If you fail to pay or meet any difficulties, please contact our staff <a href="/customer-support" target="_blank"> here.</a>.
                                    </li>
                                </ol>
                                For other payment information, please check  <a href="/payment-method" target="_blank">payment methods</a>.<br>
                            </div>
                            <div class="col-md-2 hidden-sm"></div>
                        </div>
                                            </div>
                </div>
                                <div id="confirmation-popup" class="white-popup mfp-hide">
                    <div class="confirmation-header">Your payment will be expired in 10 minutes.</div>
                    <div class="pop-confirmation-message"></div>
                    <div class="row">
                        <div class="col-xs-6"><a href="javascript:void(0)" class="btn-big-red pull-right dissagree-extend" >No</a></div>
                        <div class="col-xs-6"><a class="btn-big-red pull-right agree-extend" href="javascript:void(0)" >Yes</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<footer id="footer">
    <div class="footer-copyright">
        <div class="container">
            &copy; Copyright 2018 - digitalcreativeconsultant.com
        </div>
    </div>
</footer>
    <div id="userform" class="white-popup mfp-with-anim mfp-hide">
    <div class="popup-userform">
        <div class="tab-form">
            <a href="#form-login">Log In</a>
            <a href="#form-register">Sign Up</a>
        </div>
        <div id="form-login">
            <div class="flash-msg text-center"></div>
            <form method="POST" action="https://www.indohotels.id/payment-status" accept-charset="UTF-8" id="ids-login-form"><input name="_token" type="hidden" value="R7EkjpBWNQDmJJwn8ZwIUTF7w5ienLtG35DLzexP">
            <div class="form-group">
                <label for="">Email</label>
                <input type="text" name="username" class="form-control" placeholder="">
            </div>
            <div class="form-group">
                <label for="">Password</label>
                <input type="password" name="password" class="form-control" placeholder="">
            </div>
            <div class="form-group">
                <div class="checkbox clearfix">
                    <label> <input type="checkbox">Remember Me</label>
                    <a href="https://www.indohotels.id/forget-password" class="pull-right">Forgot Password</a>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-red-fullwidth">Log In</button>
            </div>
            </form>
        </div>
        <div id="form-register">
            <div class="flash-msg text-center"></div>
            <form method="POST" action="https://www.indohotels.id/payment-status" accept-charset="UTF-8" id="ids-register-form"><input name="_token" type="hidden" value="R7EkjpBWNQDmJJwn8ZwIUTF7w5ienLtG35DLzexP">
            <div class="form-group">
                <label for="">First name</label>
                <input name="firstname" type="text" class="form-control" placeholder="">
            </div>
            <div class="form-group">
                <label for="">Last name</label>
                <input name="lastname" type="text" class="form-control" placeholder="">
            </div>
            <div class="form-group">
                <label for="">Email</label>
                <input name="email" type="email" class="form-control" placeholder="">
            </div>
            <div class="form-group">
                <label for="">Password</label>
                <input name="password" type="password" class="form-control" placeholder="">
            </div>
            <div class="form-group">
                <div class="checkbox">
                    <label>
                        <input id="agree" type="checkbox">
                        I have read and agree to the <span class=""><a href="https://www.indohotels.id/terms-conditions">Terms of Service</a></span>
                    </label>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-red-fullwidth">Register</button>
            </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://www.indohotels.id/js/jquery.redirect.js"></script>
<script type="text/javascript" src="https://www.indohotels.id/asset/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://www.indohotels.id/asset/js/vendor/jquery.collapse.js"></script>
<script type="text/javascript" src="https://www.indohotels.id/asset/js/vendor/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="https://www.indohotels.id/asset/js/vendor/slick.min.js"></script>
<script type="text/javascript" src="https://www.indohotels.id/asset/js/vendor/jquery.scrollToTop.min.js"></script>
<script type="text/javascript" src="https://www.indohotels.id/asset/js/global.min.js?v=2.0"></script>
<script type="text/javascript" src="https://www.indohotels.id/js/ids-js.js?v=2.0"></script>
<script type="text/javascript" src="https://www.indohotels.id/js/js.cookie.js"></script>

    
<script type="text/javascript" src="https://www.indohotels.id/js/jquery.countdown.min.js"></script>
<script>
        $('#clock').countdown( '2018/08/26 22:59:14' )
            .on('update.countdown', function(event) {
                if(event.strftime('%S') == 30)
                {
                    var reload_status = Cookies.get('reload_status');
                    if ((typeof reload_status == 'undefined') && (reload_status != 1) && (event.strftime('%M') == 10)){
                        $('.pop-confirmation-message').html('Do yo want to extend ?');
                        $.magnificPopup.open({
                            items: {
                                src: '#confirmation-popup'
                            },
                            type: 'inline'
                        })
                    }else{
                        location.reload();
                    }
                }
                var $this = $(this).html(event.strftime('<span>%M</span> min <span>%S</span> sec'));
            }).on('finish.countdown', function(event) {
                location.reload();
            });
</script>
<script>
        $('.dissagree-extend').click(function() {
            $.magnificPopup.close();
            location.reload();
        });

        $('.agree-extend').click(function() {
            // Cookies.set('reload_status', 1);
            // Cookies.set('booking_id', '56fd4730-a93e-11e8-8a22-99cc1da16505');
            // Cookies.set('booking_order_number', 'IDSFDC11535294594');

            // $.ajax({
            //     type: 'get',
            //     url: 'https://www.indohotels.id/payment-extend-time',
            //     data: {
            //         'booking_id':Cookies.get('booking_id'),
            //         'booking_order_number':Cookies.get('booking_order_number'),
            //     },
            //     success: function (r) {
            //         $.magnificPopup.close();
            //         location.reload();
            //     }
            // });

            window.location.reload();
        });
</script>

</body>
</html>
