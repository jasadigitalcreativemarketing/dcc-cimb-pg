<?php

    service(file_get_contents('php://input'));

    function service($bodyContent) {
        global $wpdb;
        $va_log = $wpdb->prefix . "dcc_cimb_va_log";

        $extract = str_ireplace(['soapenv:', 'ser:', 'bil:', 'SOAP-ENV:', 'm:', '<MessagePayload>', '</MessagePayload>'] , '' , $bodyContent);
        $form_backup = [
            'request_detail' => json_encode($bodyContent)
        ];
        
        // Save all request that post to this table
        // $wpdb->insert( $va_log, array(
	    //     'va_log' => $form_backup['request_detail'],
		// )); 
        
        $xml = new SimpleXMLElement($extract);
        
        $get_request_type = $xml->Body->children()->children()->getName();
        
        if ( $get_request_type == 'EchoRequest' )
        {
            header("Content-type: text/xml");

            $echo_response = '<soap:Envelope xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">';
            $echo_response .= '<soap:Body>';
            $echo_response .= '<CIMB3rdParty_EchoRs xmlns="https://payment.indohotels.id:8005/service">';
            $echo_response .= '<EchoResponse>';
            $echo_response .=  $xml->Body->children()->children();
            $echo_response .= '</EchoResponse>';
            $echo_response .= '</CIMB3rdParty_EchoRs>';
            $echo_response .= '</soap:Body>';
            $echo_response .= '</soap:Envelope>';
            
            $wpdb->insert( $va_log, array(
                'va_log' => json_encode($echo_response),
            )); 
            
            return $echo_response;
        }
        
        if ( $get_request_type == 'InquiryRq' )
        {
            if (empty( $xml->Body->CIMB3rdParty_InquiryRq->InquiryRq->CustomerKey1 ) || $xml->Body->CIMB3rdParty_InquiryRq->InquiryRq->CustomerKey1 == "?" )
            {
                $get_booking_valid = generateError('11','No Customer Key');
            }
            else if ( empty( $xml->Body->CIMB3rdParty_InquiryRq->InquiryRq->CompanyCode ) || $xml->Body->CIMB3rdParty_InquiryRq->InquiryRq->CompanyCode == "?" )
            {
                $get_booking_valid = generateError('10','CompanyCode Empty');
            }
            else if ( empty( $xml->Body->CIMB3rdParty_InquiryRq->InquiryRq->TransactionDate ) || $xml->Body->CIMB3rdParty_InquiryRq->InquiryRq->TransactionDate == "?" )
            {
                $get_booking_valid = generateError('14','TransactionDate Empty');
            }
            else
            {
                // if ($this->checkDate( $xml->Body->CIMB3rdParty_InquiryRq->InquiryRq->TransactionDate ) == false)
                // {
                //     $get_booking_valid = generateError('15','TransactionDate format not match');
                // }
                // else
                // {
                //     $get_booking_valid = checkBookByVa(  $xml->Body->CIMB3rdParty_InquiryRq->InquiryRq->CompanyCode.$xml->Body->CIMB3rdParty_InquiryRq->InquiryRq->CustomerKey1 );

                //     if (isset($get_booking_valid['status_adv']) && $get_booking_valid['status_adv']== 'bill_not_found')
                //     {
                //         $get_booking_valid = generateError('40','Bill Not Found');
                //     }

                //     //additional validation
                //     $response = $this->searchReference($xml->Body->CIMB3rdParty_InquiryRq->InquiryRq->CompanyCode.$xml->Body->CIMB3rdParty_InquiryRq->InquiryRq->CustomerKey1);

                //     if ( $response['message'] == 2 )
                //     {
                //         $get_booking_valid = generateError('41' , 'Bill Already Paid');
                //     }

                //     if ( $response['message'] == 4 )
                //     {
                //         $get_booking_valid = generateError('45','Bill Expired');
                //     }

                // }
            }
            print_r($get_booking_valid);

        //     header("Content-type: text/xml");

        //     $enquiry_response = '<soap:Envelope xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">';
        //     $enquiry_response .= '<soap:Body>';
        //     $enquiry_response .= '<CIMB3rdParty_InquiryRs xmlns="https://payment.indohotels.id:8005/service">';
        //     $enquiry_response .= '<InquiryRs>';
        //     $enquiry_response .= '<TransactionID>';
        //     $enquiry_response .=  $xml->Body->CIMB3rdParty_InquiryRq->InquiryRq->TransactionID;
        //     $enquiry_response .= '</TransactionID>';
        //     $enquiry_response .= '<ChannelID>';
        //     $enquiry_response .=  $xml->Body->CIMB3rdParty_InquiryRq->InquiryRq->ChannelID;
        //     $enquiry_response .= '</ChannelID>';
        //     $enquiry_response .= '<TerminalID>';
        //     $enquiry_response .=  $xml->Body->CIMB3rdParty_InquiryRq->InquiryRq->TerminalID;
        //     $enquiry_response .= '</TerminalID>';
        //     $enquiry_response .= '<TransactionDate>';
        //     $enquiry_response .=  $xml->Body->CIMB3rdParty_InquiryRq->InquiryRq->TransactionDate;
        //     $enquiry_response .= '</TransactionDate>';
        //     $enquiry_response .= '<CompanyCode>';
        //     $enquiry_response .=  config('services.payment.company_code');
        //     $enquiry_response .= '</CompanyCode>';
        //     $enquiry_response .= '<CustomerKey1>';
        //     $enquiry_response .=  $xml->Body->CIMB3rdParty_InquiryRq->InquiryRq->CustomerKey1;
        //     $enquiry_response .= '</CustomerKey1>';
        //     $enquiry_response .= '<CustomerKey2>';
        //     $enquiry_response .=  $xml->Body->CIMB3rdParty_InquiryRq->InquiryRq->CustomerKey2;
        //     $enquiry_response .= '</CustomerKey2>';
        //     $enquiry_response .= '<CustomerKey3>';
        //     $enquiry_response .=  $xml->Body->CIMB3rdParty_InquiryRq->InquiryRq->CustomerKey3;
        //     $enquiry_response .= '</CustomerKey3>';
        //     $enquiry_response .= '<BillDetailList>';
        //     $enquiry_response .= '<BillDetail>';
        //     $enquiry_response .= '<BillCurrency>IDR</BillCurrency>';
        //     $enquiry_response .= '<BillCode>'.$get_booking_valid['bill_reference'].'</BillCode>';
        //     $enquiry_response .= '<BillAmount>'.$get_booking_valid['amount'].'</BillAmount>';
        //     $enquiry_response .= '<BillReference>'.$get_booking_valid['bill_reference'].'</BillReference>';
        //     $enquiry_response .= '</BillDetail>';
        //     $enquiry_response .= '</BillDetailList>';
        //     $enquiry_response .= '<Currency>IDR</Currency>';
        //     $enquiry_response .= '<Amount>'.$get_booking_valid['amount'].'</Amount>';
        //     $enquiry_response .= '<PaidAmount>'.$get_booking_valid['amount'].'</PaidAmount>';
        //     $enquiry_response .= '<CustomerName>'.$get_booking_valid['customer_name'].'</CustomerName>';
        //     $enquiry_response .= '<AdditionalData1/>';
        //     $enquiry_response .= '<AdditionalData2/>';
        //     $enquiry_response .= '<AdditionalData3/>';
        //     $enquiry_response .= '<AdditionalData4/>';
        //     $enquiry_response .= '<FlagPayment>100000</FlagPayment>';
        //     $enquiry_response .= '<ResponseCode>'.$get_booking_valid['response_code'].'</ResponseCode>';
        //     $enquiry_response .= '<ResponseDescription>'.$get_booking_valid['response_details'].'</ResponseDescription>';
        //     $enquiry_response .= '</InquiryRs>';
        //     $enquiry_response .= '</CIMB3rdParty_InquiryRs>';
        //     $enquiry_response .= '</soap:Body>';
        //     $enquiry_response .= '</soap:Envelope>';

        //     $backup_response = json_decode( ApiCaller::token( config( 'services.api.token' ) )->post( 'transaction/do-backup-va', ['request_detail'=>json_encode($enquiry_response)] ), true );
        //     return $enquiry_response;
        }

        // if ( $get_request_type == 'PaymentRq' )
        // {

        //     if ( $xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->CompanyCode == "?" || empty($xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->CompanyCode) )
        //     {
        //         $get_booking_valid = $this->generateError('21','Company Code Empty');
        //     }
        //     else if (!isset( $xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->CustomerKey1 ))
        //     {
        //         $get_booking_valid = $this->generateError('22','No Customer Key');
        //     }
        //     else if ( $xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->TransactionID == "?" || empty( $xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->TransactionID ))
        //     {
        //         $get_booking_valid = $this->generateError('23','Transaction ID Empty');
        //     }
        //     else if ( $xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->TransactionDate == "?" || empty( $xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->TransactionDate ))
        //     {
        //         $get_booking_valid = $this->generateError('25','Transaction Date Empty');
        //     }
        //     else
        //     {
        //         $response = $this->searchReference($xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->CompanyCode.$xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->CustomerKey1);

        //         // return $xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->CompanyCode.$xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->CustomerKey1;

        //         if ( !isset( $response['message'] ) )
        //         {
        //             $get_booking_valid = $this->generateError('32','Bill Not Found');
        //         }
        //         else
        //         {
        //             //additional
        //             if ($this->checkDate( $xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->TransactionDate ) == false)
        //             {
        //                 $get_booking_valid = $this->generateError('26','TransactionDate format not match');
        //             }

        //             if ( $response['message'] == 1)
        //             {
        //                 $get_booking_valid = $this->checkBookByVa( $xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->CompanyCode.$xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->CustomerKey1 , $xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->PaidAmount ,true );
        //                 // return $get_booking_valid['bill_reference'];
        //             }
        //             else if ( $response['message'] == 2 )
        //             {
        //                 $get_booking_valid = $this->generateError('33' , 'Bill Already Paid');
        //             }
        //             else if ( $response['message'] == 4 )
        //             {
        //                 $get_booking_valid = $this->generateError('37','Bill Expired');
        //             }
        //             else
        //             {
        //                 $get_booking_valid = $this->generateError('32','Bill Not Found');
        //             }
        //         }
        //     }

        //     if ( $get_booking_valid['response_code'] == '00' )
        //     {
        //         $form_success = [
        //             'order_number' => $get_booking_valid['bill_reference']
        //         ];
        //         $successTransaction = json_decode( ApiCaller::token( config( 'services.api.token' ) )->post('transaction/success-payment', $form_success ), true );
        //     }

        //     header("Content-type: text/xml");

        //     $payment_response = '<soap:Envelope xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">';
        //     $payment_response .= '<soap:Body>';
        //     $payment_response .= '<CIMB3rdParty_PaymentRs xmlns="https://payment.indohotels.id:8005/service">';
        //     $payment_response .= '<PaymentRs>';
        //     $payment_response .= '<TransactionID>';
        //     $payment_response .=  $xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->TransactionID;
        //     $payment_response .= '</TransactionID>';
        //     $payment_response .= '<ChannelID>';
        //     $payment_response .=  $xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->ChannelID;
        //     $payment_response .= '</ChannelID>';
        //     $payment_response .= '<TerminalID>';
        //     $payment_response .=  $xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->TerminalID;
        //     $payment_response .= '</TerminalID>';
        //     $payment_response .= '<TransactionDate>';
        //     $payment_response .=  $xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->TransactionDate;
        //     $payment_response .= '</TransactionDate>';
        //     $payment_response .= '<CompanyCode>';
        //     $payment_response .=  config('services.payment.company_code');
        //     $payment_response .= '</CompanyCode>';
        //     $payment_response .= '<CustomerKey1>';
        //     $payment_response .=  $xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->CustomerKey1;
        //     $payment_response .= '</CustomerKey1>';
        //     $payment_response .= '<CustomerKey2>';
        //     $payment_response .=  $xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->CustomerKey2;
        //     $payment_response .= '</CustomerKey2>';
        //     $payment_response .= '<CustomerKey3>';
        //     $payment_response .=  $xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->CustomerKey3;
        //     $payment_response .= '</CustomerKey3>';
        //     $payment_response .= '<PaymentFlag>100000';
        //     $payment_response .= '</PaymentFlag>';
        //     $payment_response .= '<CustomerName>'.$get_booking_valid['customer_name'].'</CustomerName>';
        //     $payment_response .= '<Currency>IDR</Currency>';
        //     $payment_response .= '<Amount>'.$get_booking_valid['amount'].'</Amount>';
        //     $payment_response .= '<Fee>0</Fee>';
        //     $payment_response .= '<PaidAmount>'.$xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->PaidAmount.'</PaidAmount>';
        //     $payment_response .= '<ReferenceNumberTransaction>';
        //     $payment_response .=  isset($xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->ReferenceNumberTransaction)? $xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->ReferenceNumberTransaction : '';
        //     $payment_response .= '</ReferenceNumberTransaction>';
        //     $payment_response .= '<AdditionalData1>';
        //     $payment_response .=  isset($xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->AdditionalData1)? $xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->AdditionalData1 : '';
        //     $payment_response .= '</AdditionalData1>';
        //     $payment_response .= '<AdditionalData2>';
        //     $payment_response .=  isset($xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->AdditionalData2)? $xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->AdditionalData2 : '';
        //     $payment_response .= '</AdditionalData2>';
        //     $payment_response .= '<AdditionalData3>';
        //     $payment_response .=  isset($xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->AdditionalData3)? $xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->AdditionalData3 : '';
        //     $payment_response .= '</AdditionalData3>';
        //     $payment_response .= '<AdditionalData4>';
        //     $payment_response .=  isset($xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->AdditionalData4)? $xml->Body->CIMB3rdParty_PaymentRq->PaymentRq->AdditionalData4 : '';
        //     $payment_response .= '</AdditionalData4>';
        //     $payment_response .= '<ResponseCode>'.$get_booking_valid['response_code'].'</ResponseCode>';
        //     $payment_response .= '<ResponseDescription>'.$get_booking_valid['response_details'].'</ResponseDescription>';
        //     $payment_response .= '</PaymentRs>';
        //     $payment_response .= '</CIMB3rdParty_PaymentRs>';
        //     $payment_response .= '</soap:Body>';
        //     $payment_response .= '</soap:Envelope>';

        //     $backup_response = json_decode( ApiCaller::token( config( 'services.api.token' ) )->post( 'transaction/do-backup-va', ['request_detail'=>json_encode($payment_response)] ), true );

        //     return $payment_response;
        // }
    }


    // /**
    // *   get list book from this VA and validate
    // */

    // function checkBookByVa($id, $amount=0 ,$payment=null)
    // {
    //     $return['amount'] = 0;
    //     $return['response_code'] = '99';
    //     $return['response_details'] = 'General Failure';
    //     $return['customer_name'] = '';
    //     $return['bill_reference'] = '';
    //     $return['details'] = '';
    //     $all_validation = true;

    //     $getTransaction = json_decode( ApiCaller::token( config( 'services.api.token' ) )->get('transaction/get-booking-by-va/'.$id), true );
    //     $return['details'] = $getTransaction;

    //     // return $return['details'];

    //     if ( count( $getTransaction['data'] ) )
    //     {
    //         foreach ( $getTransaction['data'] as $transaction )
    //         {
    //             $validation = $this->checkValid($transaction);

    //             $adv_property_details = ParseArrayJson::parseArray($transaction['booking_properties_detail']);

    //             if ( $validation['status'] != "error" )
    //             {
    //                 $return['amount'] += $transaction['booking_payment'];
    //                 $return['customer_name'] = $transaction['booking_customer_name'];
    //                 $return['bill_reference'] = $transaction['booking_order_number'];

    //                 $return['response_code'] = '00';
    //                 $return['response_details'] = 'Transaction Success';

    //                 $property_options = ParseArrayJson::parseArray($adv_property_details['property_special_condition']);
    //                 $web_id = $transaction['booking_website'];

    //                 if (!empty($property_options['uptransfer']))
    //                 {
    //                     $admin_fee = AdminFee::transfer($property_options, (int)$return['amount'], (int)$web_id);
    //                     $return['amount'] += $admin_fee;
    //                 }
    //             }
    //             else
    //             {
    //                 foreach ( $getTransaction['data'] as $transaction )
    //                 {

    //                     $setNotValid = json_decode( ApiCaller::token( config( 'services.api.token' ) )->get( 'transaction/book-status-not-sync/'.$transaction['booking_id'] ), true );
    //                 }

    //                 $all_validation = false;
    //                 $return['status'] = "error";
    //                 $return['response_code'] = '45';
    //                 $return['response_details'] = 'Bill Expired';
    //                 return $return;
    //             }
    //         }

            

    //     }
    //     else
    //     {
    //         $return['status'] = "error";
    //         $return['status_adv'] = "bill_not_found";
    //         $return['response_code'] = '45';
    //         $return['response_details'] = 'Bill Expired';
    //         return $return;
    //     }

    //     if ( $amount != 0 || $amount !="?")
    //     {
    //         if ($amount != $return['amount'])
    //         {
    //             $return['status'] = "error";
    //             $return['response_code'] = '38';
    //             $return['response_details'] = 'Invalid Amount';
    //             return $return;
    //         }
    //     }

    //     if ($payment == true)
    //     {
    //         if ($amount != $return['amount'])
    //         {
    //             $return['status'] = "error";
    //             $return['response_code'] = '38';
    //             $return['response_details'] = 'Invalid Amount';
    //             return $return;
    //         }
    //     }

    //     return $return;
    // }



    // /**
    // *check whether book still valid or not
    // **/
    // function checkValid ( $transaction_details )
    // {
    //     $data ['total_price'] = 0;
    //     $data ['total_price_db'] = 0;
    //     $data ['website_price'] = 0;
    //     $option = array();

    //     $getTransaction['data']  = $transaction_details;

    //     if ( isset( $getTransaction['data'] ) )
    //     {
    //         $data['night'] = ( $getTransaction['data']['booking_checkout_date'] - $getTransaction['data']['booking_checkin_date'] ) / ( 60 * 60 * 24 );

    //         $data['total_price_db'] += $getTransaction['data']['booking_payment'];

    //         $transaction_decode = ParseArrayJson::parseArray( $getTransaction['data']['booking_cookie'] );
    //         $transaction_booking_website = $getTransaction['data']['booking_website'];

    //         $data['room_type'] = $transaction_decode['room_type'];

    //         /*extract data new price */
    //         $mobileStatus = 0;
    //         $data['membership_status'] = 0;
    //         $data['membership_code'] = 0;

    //         // return $transaction_decode['new_price'];

    //         if (isset($transaction_decode['new_price']))
    //         {
    //             $data['by_new_price'] = $transaction_decode['new_price'];
    //             foreach ($data['by_new_price'] as $value)
    //             {
    //                 if (isset($value['mobile']))
    //                 {
    //                     $mobileStatus = $value['mobile'];
    //                 }
    //             }

    //             if ( isset($transaction_decode['membership']) && $transaction_decode['membership'] > '0' )
    //             {
    //                 $form = [
    //                     'voucher_property_id' => $getTransaction['data']['booking_property_id'],
    //                     'voucher_number' => $transaction_decode['membership'],
    //                     'checkin' => (isset($getTransaction['data']['booking_checkin_date'])) ? (date('d-m-Y',$getTransaction['data']['booking_checkin_date'])) : (gmdate('d-m-Y', time()+25200)),
    //                     'checkout' => (isset($getTransaction['data']['booking_checkout_date'])) ? (date('d-m-Y',$getTransaction['data']['booking_checkout_date'])) : (gmdate('d-m-Y', time() + (60 * 60 * 24) +25200)),
    //                 ];


    //                 $membership = json_decode(ApiCaller::token( config( 'services.api.token' ) )->post('search/membership-code/', $form), true);

    //                 if(isset($membership['message']) && $membership['message']=='v')
    //                 {
    //                     $data['membership_status'] = 1;
    //                     $data['membership_code'] = $transaction_decode['membership'];
    //                 }

    //                 // limited discount
    //                 if (isset($membership['option']) && $membership['option'] == 'limited')
    //                 {
    //                     $data['membership_status']  = 0;
    //                     $data['limited_status']     = 1;
    //                     $data['membership_code']    = $transaction_decode['membership'];
    //                     $option   = [
    //                         'limited_data' =>  isset($membership['limited_data']) ? $membership['limited_data'] : null,
    //                         'limited_data_details' => isset($membership['list_room']) ? $membership['list_room'] : null,
    //                     ];
    //                 }
    //             }
    //         }

    //         /*end new price*/

    //         foreach ($transaction_decode['room_type'] as $key => $value)
    //         {
    //             $form = [
    //                 'room_id' => $key,
    //                 'room_qty' => $value,
    //                 'checkin' => isset(  $getTransaction['data']['booking_checkin_date'] ) ?
    //                 ( $getTransaction['data']['booking_checkin_date'] ) : "",
    //                 'checkout' => isset( $getTransaction['data']['booking_checkout_date'] ) ?
    //                 ( $getTransaction['data']['booking_checkout_date'] ) : "",
    //             ];

    //             $roomPrice[] = json_decode( ApiCaller::token( config( 'services.api.token' )  )->post('search/room-price/', $form), true );
    //         }

    //         if (!isset($roomPrice)){
    //             $return['status'] = 'error';
    //             $return['cookie'] = $getTransaction['data']['booking_cookie'];
    //             $return['message'] = 'This room(s) already sold a minute ago. we remove it from your cart. please check again.';
    //             return $return;
    //         }

    //         if ( isset( $roomPrice ) )
    //         {
    //             foreach ( $roomPrice as $roomData )
    //             {
    //                 if ( isset( $roomData['message'] ) )
    //                 {
    //                     $return['status'] = 'error';
    //                     $return['cookie'] = $getTransaction['data']['booking_cookie'];
    //                     $return['message'] = 'This room(s) already sold a minute ago. we remove it from your cart. please check again.';
    //                     return $return;
    //                 }
    //             }
    //         }

    //         foreach ( $roomPrice as $roomData )
    //         {
    //             $room = $roomData['data'];
    //             $data[ 'room_type' ][ $room['room_id'] ][ 'name' ] = $room['room_name'];
    //             $data[ 'room_type' ][ $room['room_id'] ][ 'count' ] = count( $room['allotment'] );

    //             //ids discount per room
    //             if (!empty($transaction_decode['ids_discount'])) {
    //                 $disc_form = [
    //                     'code' => $transaction_decode['ids_discount'],
    //                     'booking_date' => Carbon::today('Asia/jakarta')->format('Y-m-d'),
    //                     'stay_start' => Carbon::createFromFormat('d-m-Y H', $transaction_decode['start'].' 0', 'Asia/Jakarta')->format('Y-m-d'),
    //                     'stay_end' => Carbon::createFromFormat('d-m-Y H', $transaction_decode['end'].' 0', 'Asia/Jakarta')->format('Y-m-d'),
    //                     'property_id' => $transaction_decode['property']['property_id'],
    //                     'star' => $transaction_decode['property']['property_stars'],
    //                     'room_id' => ParseArrayJson::parseArray($transaction_decode['room_list']),
    //                     'room_max' =>  $transaction_decode['room_max'],
    //                     'room_min' =>  $transaction_decode['room_min'],
    //                     'per_room' =>  $room['room_id'],
    //                     'web_id' => $transaction_booking_website,
    //                 ];
        
    //                 $discount = IdsDiscount::getByCodeRoom($disc_form);
        
    //                 if (isset($discount['data']['upto_percent'])){
    //                     $list_ids_upto_percent = $discount['data']['upto_percent'];
    //                 }
        
    //                 if (isset($list_ids_upto_percent)){
    //                     if (isset($discount['data']['list_discount']) && count($discount['data']['list_discount'] > 0)){
    //                         // $index =  array_search($list_ids_upto_percent['max_id'], array_column($discount['data']['list_discount'], 'discount_id'));
    //                         $index = ArraySearch::whereIndex($discount['data']['list_discount'], 'discount_id', $list_ids_upto_percent['max_id']);
    //                         $list_ids_discount =  $discount['data']['list_discount'][$index];
    //                         $option['ids_discount'] = $list_ids_discount;
    //                     }
    //                 }
    //             }

    //             if ( $getTransaction['data']['booking_website'] > 100 )
    //             {
    //                 if ( isset($room['property']['property_website_discount']) AND !empty($room['property']['property_website_discount']) )
    //                 {
    //                     $data ['website_price'] = $room['property']['property_website_discount'];
    //                 }
    //             }

    //             /* new function price */
    //             $data['new_price'][$room['room_id']] = getPriceDetails($room, $mobileStatus, $data['night'], (int)$data['room_type'][$room['room_id']]['qty'],  $data['room_type'][$room['room_id']]['extra_qty'],  $data['membership_status'] , $room['property'], $getTransaction['data']['booking_date'], $data ['website_price'], $option );

    //             if (isset($data['new_price']['room_not_available']) && $data['new_price']['room_not_available'] == 1)
    //             {
    //                 $return['status'] = 'error';
    //                 $return['cookie'] = $getTransaction['data']['booking_cookie'];
    //                 $return['message'] = 'room price not available please check again. we remove it from your cart. please check again.';
    //                 return $return;
    //             }
    //             /* end new function */

    //             if ( isset( $room ) && count( $room['allotment'] ) >= $data['night'] )
    //             {
    //                 foreach ($room['allotment'] as $allotment)
    //                 {
    //                     // check availability for total n night
    //                     // checking data using populate data before. automatic redirect if
    //                     //room not available
    //                     $quota = array();
    //                     $quota_price_each_room = array();

    //                     foreach ( $allotment['price'] as $price )
    //                     {
    //                         $data[ 'room_avail_days' ][ $allotment['allotment_date'] ][ $room['room_id'] ][]  = $price;
    //                         // populate data for availability group by room id and date
    //                         $quota[] = $price['price_quota']-$price['price_used'];
    //                         $quota_price_each_room[] = [
    //                             'quota' => $price['price_quota']-$price['price_used'],
    //                             'price_nominal' => $price['price_nominal'],
    //                             'price_id' => $price['price_id'],
    //                             'price_allotment_id' => $price['price_allotment_id']
    //                         ];

    //                     }

    //                     $data['check_avail'][ $room['room_id'] ][ $allotment['allotment_date'] ] = array_sum($quota);

    //                     if ( $data['check_avail'][ $room['room_id'] ][ $allotment['allotment_date'] ] < 1){
    //                         //return fail room not avail for n night
    //                         $return['status'] = 'error';
    //                         $return['cookie'] = $getTransaction['data']['booking_cookie'];
    //                         $return['message'] = 'room not available for this range date.'
    //                         .' we remove it from your cart. please check again.';
    //                         return $return;
    //                     }

    //                     $count = 1;
    //                     $count_each_price = 0;
    //                     sort($quota_price_each_room);
    //                     $for_price = 0;

    //                     while ( $count <= $data['room_type'][$room['room_id']]['qty'])
    //                     {
    //                         if ( !isset( $quota_price_each_room[ $for_price ]['quota'] ) )
    //                         {
    //                             $return['status'] = 'error';
    //                             $return['cookie'] = $getTransaction['data']['booking_cookie'];
    //                             $return['message'] = 'room not available for this range date. we remove it from your cart. please check again.';
    //                             return $return;
    //                         }

    //                         if ($quota_price_each_room[ $for_price ]['quota'] > 0)
    //                         {
    //                             $count++;
    //                             $count_each_price++;
    //                             $quota_price_each_room[ $for_price ]['quota'] -= 1;
    //                             //count details each price
    //                             $data['detail_price'][ $allotment['allotment_date'] ][$room['room_id']]['room_price'][ $quota_price_each_room[ $for_price ]['price_nominal'] ] = $count_each_price;
    //                             $data['detail_price'][ $allotment['allotment_date'] ][$room['room_id']]['room_price_id'][ $quota_price_each_room[ $for_price ]['price_nominal'] ] = $quota_price_each_room[ $for_price ]['price_id'];
    //                             $data['detail_price'][ $allotment['allotment_date'] ][$room['room_id']]['room_allotment_id'][ $quota_price_each_room[ $for_price ]['price_nominal'] ] = $quota_price_each_room[ $for_price ]['price_allotment_id'];
    //                             $data['detail_price'][ $allotment['allotment_date'] ][$room['room_id']]['extra_bed_price'] = $room['room_extra_bed_price'];
    //                             $data['detail_price'][ $allotment['allotment_date'] ][$room['room_id']]['extra_bed_qty'] = $data['room_type'][$room['room_id']]['extra_qty'];
    //                             //total-price
    //                             $data['total_price'] += $quota_price_each_room[ $for_price ]['price_nominal'];
    //                         }
    //                         else
    //                         {
    //                             $count_each_price=0;
    //                             $for_price++;
    //                         }
    //                     }
    //                     $data['total_price'] += $data['detail_price'][ $allotment['allotment_date'] ][$room['room_id']]['extra_bed_price'] * (int)$data['detail_price'][ $allotment['allotment_date'] ][$room['room_id']]['extra_bed_qty'];
    //                 }

    //                 //make sure book not more than minimal quota each night
    //                 $data['room_minimal_quota_each_night'][ $room['room_id'] ] = min( $data['check_avail'][ $room['room_id'] ] );
    //                 if ( $data['room_minimal_quota_each_night'][ $room['room_id'] ] < $data['room_type'][$room['room_id']]['qty'] ){
    //                     //return fail room not avail for n night
    //                     $return['status'] = 'error';
    //                     $return['cookie'] = $getTransaction['data']['booking_cookie'];
    //                     $return['message'] = 'room not available for this range date. we remove it from your cart. please check again.';
    //                     return $return;
    //                 }

    //             }
    //             else
    //             {
    //                 //return fail room not avail for n night
    //                 $return['status'] = 'error';
    //                 $return['cookie'] = $getTransaction['data']['booking_cookie'];
    //                 $return['message'] = 'room not available for this range date. we remove it from your cart. please check again.';
    //                 return $return;
    //             }
    //         }

    //         /*apply new price */
    //         if(isset($data['new_price']))
    //         {
    //             $data['total_price'] = 0;
    //             $data['total_price_extra_bed'] = 0;
    //             foreach ($data['new_price'] as $new_price)
    //             {
    //                 $data['total_price'] += $new_price['display_price'];
    //                 if (isset( $new_price['extra_bed_price'] ))
    //                 {
    //                     $data['total_price_extra_bed'] += $new_price['extra_bed_price'] * $new_price['extra_bed'];
    //                 }
    //             }
    //         }
    //         /* end new function */

    //         if ( $data['total_price'] != $data['total_price_db'] )
    //         {
    //             $return['status'] = 'error';
    //             $return['cookie'] = $getTransaction['data']['booking_cookie'];
    //             $return['message'] = 'there is an update price. we remove it from your cart. please check again.';
    //             return $return;
    //         }

    //         $return['status'] = 'valid';
    //         $return['book_id'] = $getTransaction['data']['booking_id'];
    //         return $return;
    //     }

    // }

    // function noCustomerKey()
    // {
    //     $return['amount'] = 0;
    //     $return['response_code'] = '';
    //     $return['response_details'] = '';
    //     $return['customer_name'] = '';
    //     $return['bill_reference'] = '';
    //     $return['details'] = '';
    //     $return['status'] = "error";
    //     $return['response_code'] = '22';
    //     $return['response_details'] = 'Customer Key Empty';

    //     return $return;
    // }

    // function noReferenceNumber()
    // {
    //     $return['amount'] = 0;
    //     $return['response_code'] = '';
    //     $return['response_details'] = '';
    //     $return['customer_name'] = '';
    //     $return['bill_reference'] = '';
    //     $return['details'] = '';
    //     $return['status'] = "error";
    //     $return['response_code'] = '28';
    //     $return['response_details'] = 'Reference Empty';

    //     return $return;
    // }

    // function billNotFound()
    // {
    //     $return['amount'] = 0;
    //     $return['response_code'] = '';
    //     $return['response_details'] = '';
    //     $return['customer_name'] = '';
    //     $return['bill_reference'] = '';
    //     $return['details'] = '';
    //     $return['status'] = "error";
    //     $return['response_code'] = '32';
    //     $return['response_details'] = 'Bill Not Found';

    //     return $return;
    // }

    // function billAlreadyPaid()
    // {
    //     $return['amount'] = 0;
    //     $return['response_code'] = '';
    //     $return['response_details'] = '';
    //     $return['customer_name'] = '';
    //     $return['bill_reference'] = '';
    //     $return['details'] = '';
    //     $return['status'] = "error";
    //     $return['response_code'] = '33';
    //     $return['response_details'] = 'Bill Already Paid';

    //     return $return;
    // }

    // function billExpired()
    // {
    //     $return['amount'] = 0;
    //     $return['response_code'] = '';
    //     $return['response_details'] = '';
    //     $return['customer_name'] = '';
    //     $return['bill_reference'] = '';
    //     $return['details'] = '';
    //     $return['status'] = "error";
    //     $return['response_code'] = '45';
    //     $return['response_details'] = 'Bill Expired';

    //     return $return;
    // }

    // function companyCodeEmpty()
    // {
    //     $return['amount'] = 0;
    //     $return['response_code'] = '';
    //     $return['response_details'] = '';
    //     $return['customer_name'] = '';
    //     $return['bill_reference'] = '';
    //     $return['details'] = '';
    //     $return['status'] = "error";
    //     $return['response_code'] = '21';
    //     $return['response_details'] = 'Company Code Empty';

    //     return $return;
    // }

    // function transactionIDEmpty()
    // {
    //     $return['amount'] = 0;
    //     $return['response_code'] = '';
    //     $return['response_details'] = '';
    //     $return['customer_name'] = '';
    //     $return['bill_reference'] = '';
    //     $return['details'] = '';
    //     $return['status'] = "error";
    //     $return['response_code'] = '23';
    //     $return['response_details'] = 'Transaction ID Empty';

    //     return $return;
    // }

    function generateError($code, $message)
    {
        $return['amount'] = 0;
        $return['response_code'] = '';
        $return['response_details'] = '';
        $return['customer_name'] = '';
        $return['bill_reference'] = '';
        $return['details'] = '';
        $return['status'] = "error";
        $return['response_code'] = $code;
        $return['response_details'] = $message;

        return $return;
    }

    // function searchReference( $id )
    // {
    //     $get = json_decode( ApiCaller::token( config( 'services.api.token' ) )->get('transaction/get-status-va-by-reference/'.$id), true );
    //     return $get;
    // }

    // function checkDate($date)
    // {
    //     //2016042906541234
    //     if( strlen($date) != 16)
    //     {
    //         return false;
    //     }
    //     if ( substr($date, 0, 6) != gmdate('Ym',time()+25200) )
    //     {
    //         return false;
    //     }

    //     return true;
    // }

    // function testing($id)
    // {
    //     echo "aaaa";
    //     $valid = $this->checkValid( $id );
    //     return $valid;
    // }

