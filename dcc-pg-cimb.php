<?php
/*
Plugin Name: DCC PG CIMB
Plugin URI: http://digitalcreativeconsultant.com
description: A plugin for CIMB Payment Gateway
Version: 1.0
Author: Noer
Author URI: http://digitalcreativeconsultant.com
*/

add_action('admin_menu', 'cimb_setting');

include_once 'db-query.php';
include_once 'pageTemplate.php';

function dcc_cimb_install() {
	global $wpdb;
	
	$table_name = $wpdb->prefix . "dcc_cimb_conf";
	$va_log = $wpdb->prefix . "dcc_cimb_va_log";
	$cc_log = $wpdb->prefix . "dcc_cimb_va_cc";
	
	$charset_collate = $wpdb->get_charset_collate();

	$cimb_conf = "CREATE TABLE $table_name (
		id INT NOT NULL AUTO_INCREMENT,
		merchant_acc_no VARCHAR(128) DEFAULT '' NOT NULL,
		txn_password VARCHAR(20) DEFAULT '' NOT NULL,
		company_code VARCHAR(10) DEFAULT '' NOT NULL,
		plugin_status int DEFAULT 0 NOT NULL,
		sandbox_status int DEFAULT 0 NOT NULL,
        UNIQUE KEY id (id)
	) $charset_collate;"; 
	
	$cimb_va_log = "CREATE TABLE $va_log (
		id INT NOT NULL AUTO_INCREMENT,
		va_log TEXT,
        UNIQUE KEY id (id)
    ) $charset_collate;"; 
	
	$cimb_cc_log = "CREATE TABLE $cc_log (
		id INT NOT NULL AUTO_INCREMENT,
		cc_log TEXT,
        UNIQUE KEY id (id)
    ) $charset_collate;"; 

	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	
	dbDelta( $cimb_conf );
	dbDelta( $cimb_va_log );
	dbDelta( $cimb_cc_log );

	$settingsResult = $wpdb->get_results( "SELECT * FROM $table_name WHERE id=1" );

	if ( !$settingsResult ) {
		$wpdb->insert( $table_name, array(
			'id' => 1,
	        'merchant_acc_no' => '',
	        'txn_password' => '',
			'company_code' => '',
			'plugin_status' => 0,
			'sandbox_status' => 0
		));  
	} 
	
}
register_activation_hook( __FILE__, 'dcc_cimb_install' );


function dcc_cimb_uninstall() {
	global $wpdb;

	$table_name = $wpdb->prefix . "dcc_cimb_conf";
	$va_log = $wpdb->prefix . "dcc_cimb_va_log";
	$cc_log = $wpdb->prefix . "dcc_cimb_va_cc";

	$wpdb->query("DROP TABLE IF EXISTS $table_name");
	$wpdb->query("DROP TABLE IF EXISTS $va_log");
	$wpdb->query("DROP TABLE IF EXISTS $cc_log");

}
register_uninstall_hook(__FILE__, 'dcc_cimb_uninstall');


function cimb_setting(){
        add_menu_page( 'PG CIMB Setting', 'PG CIMB Setting', 'manage_options', 'pg-cimb-setting', 'pg_cimb_setting' );
}

function pg_cimb_setting(){
	global $wpdb;
    $settings = $wpdb->get_row('SELECT * FROM '.$wpdb->prefix.'dcc_cimb_conf limit 1');
	echo "<h2>".__("CIMB Settings","dcc")."</h2>";
	//if ($settings) {
		echo "<form method='POST' action='admin-post.php'>";
		wp_nonce_field( 'dcc_cimb_settings_edit' ); 
		echo "<table class='option-table'><tr><td class='label'><strong>".__("Enable CIMB Payment Gateway:","dcc")." </strong></td><td><input type='checkbox' name='dcc_cimb_enabled' ";
		if ( isset($settings->plugin_status) && $settings->plugin_status ) { echo "checked "; } 
		echo "value='1'> Yes</td></tr>
		<tr><td class='label'><strong>".__("Enable Sandbox mode","dcc")."</strong></td><td><input type='checkbox' name='dcc_cimb_sandbox_enabled' ";
		if ( isset($settings->sandbox_status) && $settings->sandbox_status ) { echo "checked "; } 
		echo "value='1'> Yes</td></tr>
		<tr><td class='label'><strong>".__("Merchant Account Number:","dcc")." </strong></td><td><input type='type' name='merchant_acc_code' value='$settings->merchant_acc_no' class='regular-text' /></td></tr>
		<tr><td class='label'><strong>".__("Txn Password:","dcc")." </strong></td><td><input type='type' name='txn_pass' value='$settings->txn_password' class='regular-text' /></td></tr>
		<tr><td class='label'><strong>".__("Company Code:","dcc")." </strong></td><td><input type='type' name='company_code' value='$settings->company_code' class='regular-text' /></td></tr><br>
		
		<input type='hidden' name='action' value='dcc_cimb_settings_edit' /></table>
		<br><input type='submit' value='".__("Save changes","dcc")."' class='button button-primary' class='button button-primary'>
		</form>";
	//}
}
?>